package ar.com.eximiait.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;

@RestController
public class ServiceController {
	@Value("${UNA_CLAVE:INDEFINIDO}")
	private String unaClave;

	@GetMapping("/")
	public Ejemplo home() {
		return new Ejemplo(1, unaClave);
	}
}
