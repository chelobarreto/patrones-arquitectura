FROM eclipse-temurin:17-jdk-alpine AS builder
WORKDIR /opt/app/
COPY src src/
COPY gradle gradle/
COPY build.gradle settings.gradle gradlew ./
RUN ./gradlew assemble

FROM eclipse-temurin:17-jre-alpine AS runtime
COPY --from=builder /opt/app/build/libs/app.jar /opt/app.jar
ENTRYPOINT java -jar /opt/app.jar
